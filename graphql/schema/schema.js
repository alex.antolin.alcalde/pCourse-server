import {
  GraphQLObjectType,
  GraphQLNonNull,
  GraphQLSchema,
  GraphQLString,
  GraphQLList,
  GraphQLInt,
  GraphQLBoolean
} from 'graphql/type';

import User from '../../mongoose/user'

/**
 * generate projection object for mongoose
 * @param  {Object} fieldASTs
 * @return {Project}
 */
export function getProjection (fieldASTs) {
  return fieldASTs.fieldNodes[0].selectionSet.selections.reduce((projections, selection) => {
    projections[selection.name.value] = true;
    return projections;
  }, {});
}

var userType = new GraphQLObjectType({
  name: 'user',
  description: 'user item',
  fields: () => ({
    username: {
      type: (GraphQLString),
      description: 'The name of the user.',
    },
    password: {
      type: GraphQLString,
      description: 'The password of the user.',
    },
    email: {
      type: GraphQLString,
      description: 'The email of the user.',
    },
    role: {
      type: GraphQLString,
      description: 'The role of the user.',
    }
  })
});

var schema = new GraphQLSchema({
  query: new GraphQLObjectType({
    name: 'UserQueryType',
    fields: {
      users: {
        type: new GraphQLList(userType),
        args: {
          username: {
            name: 'username',
            type: new GraphQLNonNull(GraphQLString)
          },
          password: {
            name: 'password',
            type: new GraphQLNonNull(GraphQLString)
          }
        },
        resolve: (root, {username, password}, source, fieldASTs) => {
          var projections = getProjection(fieldASTs);
          var foundItems = new Promise((resolve, reject) => {
              User.find({username, password}, projections,(err, users) => {
                  err ? reject(err) : resolve(users)
              })
          })

          return foundItems
        }
      }
    }
  })
  
});

export default schema;
