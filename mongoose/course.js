import mongoose from 'mongoose';

var Schema = mongoose.Schema;

//Create schema
var courseSchema = new Schema({
    name: String,
    completed: Boolean
}, {collection:"Courses"});

//Create model with 'courseSchema' schema
var Course = mongoose.model('Course', courseSchema);

export default Course