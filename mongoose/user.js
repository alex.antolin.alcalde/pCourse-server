import mongoose from 'mongoose';

var Schema = mongoose.Schema;

//Create schema
var userSchema = new Schema({
    username: String,
    password: String,
    email: String,
    role: String
}, {collection:"Users"});

//Create model with 'courseSchema' schema
var User = mongoose.model('User', userSchema);

export default User