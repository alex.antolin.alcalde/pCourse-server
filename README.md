# pCourse Server

## Run

* Crear una colección en nuestra base de datos local (o usar una especifica cambiando el nombre de la conexión en server.js) llamada "Courses". Recomendar Robot 3T como GUI de Mongo.

* Instalar paquetes:
```sh
$ npm install
```

* Renombrar el archivo /mongoose/config.template --> /mongoose/config.json y editar el archivo con la conexión a Mongo:
```json
{
	"host": "localhost",
	"port": "27017",
	"db": "local"
}
```

* Arrancar el servidor con nodemon:
```sh
$ npm run dev
```

* Arrancar el servidor bajo babel:
```sh
$ npm run dev3
```