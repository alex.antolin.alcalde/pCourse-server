'use strict';

import express from 'express';
import mongoose from 'mongoose';
import bodyParser from 'body-parser';

import mongo_config from './mongoose/config';
import Course from './mongoose/course';
import User from './mongoose/user';

import {graphql} from 'graphql'
import graphqlHTTP from 'express-graphql';
import schema from './graphql/Schema/Schema'

const app = express();
const MONGO_CONNECTION = `mongodb://${mongo_config.host}:${mongo_config.port}/${mongo_config.db}`;

app.use(bodyParser.urlencoded({extended:true}))
app.use(bodyParser.json())


//Conection to Mongo
mongoose.connect(MONGO_CONNECTION)
var db = mongoose.connection;
db.on('error', ()=> {console.log( '---Mongo connect failed!')})
db.once('open', () => {
	console.log( '+++Mongoose conectado')
})



//Express
app.listen(3000,()=> {console.log("+++Servidor Express levantado!!!")})

app.get('/', (req, res) =>{
	res.send("API funcionando correctamente");
})

//POST course
app.post('/course',(req,res)=>{
	console.log(req.body);
	var course = new Course({ 
		name:req.body.name,
		completed: false //Hardcoded!
	})

	course.save((err,result)=> {
		if (err) {
			console.log("---Curso ha fallado al guardar " + err)
			res.send("KO");
		}
		console.log("+++Curso guardado correctamente "+course.name)
		res.send("OK");
	})
})


app.post('/user',(req,res)=>{
	console.log(req.body);
	var user = new User({ //Hardcoded fields
		username:req.body.username,
		password:req.body.password,
		email:req.body.email,
		role: "USER" //Hardcoded!
	})

	user.save((err,result)=> {
		if (err) {
			console.log("---Curso ha fallado al guardar " + err)
			res.send("KO");
		}
		console.log("+++Curso guardado correctamente "+user.username)
		res.send("OK");
	})
})


app.use('/graphql', graphqlHTTP (req => ({
 schema
 //,graphiql:true
})))